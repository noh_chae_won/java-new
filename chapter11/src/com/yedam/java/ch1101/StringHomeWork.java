package com.yedam.java.ch1101;

import java.util.Calendar;
import java.util.Scanner;

public class StringHomeWork {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//문제1번
		String input = "a.b.c";
		
		 for (int i = input.length()-1; i>=0; i--){
	            System.out.print(input.charAt(i));
	        }
		
		
		//문제2번 문자열 개수 세기
			
		 //String str ="1a2v4b";  //숫자3개, 영어3개
				String str;
				int alpha = 0, num = 0, space = 0;
				
				Scanner scan = new Scanner(System.in);
				str = scan.nextLine();
				for(int i = 0; i <str.length(); i++)
				{
					if(('a' <= str.charAt(i) && str.charAt(i) <= 'z') 
							|| ('A' <= str.charAt(i) && str.charAt(i) <= 'Z'))
							alpha++;
					else if('0' <= str.charAt(i) && str.charAt(i) <= '9')
						num++;
					else if(' ' == str.charAt(i))	space++;			
				}
				
				System.out.println("알파벳 : " + alpha + "개");
				System.out.println("숫자 : " + num + "개");

			/* //3)입력한 생년월일 6자리를 입력하였을 경우 만 나이를 구하는 프로그램 작성
		 - 조건 -
		 금년 기준으로 100년이 넘어간 경우는 제외 한다. 또한 생일은 신경쓰지 않는다.
		 (221114 -> 1922년 11월 14일 X , 2022년 11월 14일 O)
		 ex) input : 000510, output : 21살
		 */
	 

	    
				System.out.println("만나이 계산 : "+getAge(2000,5,10));
	}
	
	 public static int getAge(int birthYear, int birthMonth, int birthDay)
	 {
	         Calendar current = Calendar.getInstance();
	        
	         int currentYear  = current.get(Calendar.YEAR);
	         int currentMonth = current.get(Calendar.MONTH) + 1;
	         int currentDay   = current.get(Calendar.DAY_OF_MONTH);
	         
	         System.out.println("현재 년도 : "+currentYear);
	         System.out.println("현재 월 : "+currentMonth);
	         System.out.println("현재 일 : "+currentDay);
	         
	         // 만 나이 구하기 2022-2000=27 (현재년-태어난년)
	         int age = currentYear - birthYear;
	         // 만약 생일이 지나지 않았으면 -1
	         if (birthMonth * 100 + birthDay > currentMonth * 100 + currentDay) 
	             age--;
	         // 5월 26일 생은 526
	         // 현재날짜 5월 25일은 525
	         // 두 수를 비교 했을 때 생일이 더 클 경우 생일이 지나지 않은 것이다.
	         return age;
	 }
	 
		}
		


