package com.yedam.java.ch1102;

public class BoxingUnboxing {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//박싱
		
		//1) 생성자를 사용한 박싱
		Integer obj1 = new Integer(100);  //사용할수 있지만 권장하지 않는다 할때 줄그어진다
		Integer obj2 = new Integer("200"); 
		
		//2) 생성자를 사용하지 않는 박싱
		Integer obj3 = Integer.valueOf("300");
		
		//언박싱
		int value1 = obj1.intValue();
		int value2 = obj2.intValue();
		int value3 = obj3.intValue();
		
		System.out.println(value1+value2+value3); //문자던 숫자던 Integer(int)배열로 들고와서 합산된다 
		System.out.println(value2);
		System.out.println(value3);
		
		//자동 박싱
		Integer obj4 = 100;
		
		//자동 언박싱
		int value4 = obj4;
		int value5 = obj4.intValue();
		
		//타입변환
		
		int value6 = Integer.parseInt("777");
		double value7 = Double.parseDouble("333");
		boolean value8 = Boolean.parseBoolean("true");
		
		System.out.println(value6);
		System.out.println(value7);
		System.out.println(value8);
		
		//포장 값 비교
		Integer obj10 = 100;
		Integer obj11 = 100; 
		System.out.println(obj10 == obj11);   //값의 비교는(300씩 주게되면)범위가(int:-128~ 최대127) 정해져있다 그걸 벗어나게 되면  
		
		
		
		
		
		
		
		
	}

}
