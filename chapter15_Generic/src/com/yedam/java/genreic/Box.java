package com.yedam.java.genreic;

public class Box<T> { //Box<T, K, A> 필요하면 1~3 쓴다 3개이상은 안쓴다 
	private T field; //타입을 담을수 있는 변수 사용 대문자 한글자를 사용한다
	
	public void set(T value) {
		this.field = value;
	}
	
	public T get() {
		return this.field;
	}
}
