package com.yedam.java.homework;

public class StandardWeightlnfo extends Human{
	
	public double standardWeight;
	
	public StandardWeightlnfo(String hName, double height, double weight) {
		super(hName, height, weight);
	}

	@Override
	public void getInformation() {
		System.out.println(gethName()+"님의 신장 "+getHeight()+", 몸무게 "+getWeight()+", 표준체중 "+getStandardWeight()+" 입니다.");
	}
	
	// 표준체중을 구하는 기능
	public double getStandardWeight() {
		 standardWeight = ((getHeight()-100) *0.9 );
		return standardWeight;
	}

}
