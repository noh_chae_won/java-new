package com.yedam.java.app;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class SQLExample {

	public static void main(String[] args) {
		Connection conn =null;
		Statement stmt=null;
		PreparedStatement ps=null;
		ResultSet rs=null;
		
		try {
			//☆★☆★JDBC 6단계 반복이다 ☆★☆★
			//1. JDBC Driver 로딩 ☆
			Class.forName("org.sqlite.JDBC"); // 드라이버 로딩하는것

			// 2. DB 서버와 접속하기
			// conn달라고 요청한다
			String url = "jdbc:sqlite:/D:/dev/database/testdatabase.db";
			conn = DriverManager.getConnection(url);
			// 2번까지는 한번만 한다 3~5는 내가 어떤걸 쓰는거에 따라서 계속 반복

			/************************* INSERT ***************************/
			// 3. Statement or PreparedStatment 객체 생성하기
			String insert = "INSERT INTO students (student_id, student_name) " + "VALUES (?, ?)"; // ? = 변수이다. 무언가 값을
																			// 예)VALUES ( 110 ,'/'"윤달하")	// 넣어줘야하는데 여기에 ? 적는다
			// 문자열 공백을 재대로 주지않으면 SQL문은 이상하게 실행된다 (적절하게 공백 주고 문법 줘야한다)
			ps = conn.prepareStatement(insert); // prepareStatement 문법이 조금다르다
			ps.setInt(1, 110); // 위치 들어가는게 첫번째 물음표에 들어가야한다 값은 뒤에
			ps.setString(2, "윤달하"); // ps.setString 후 ps.setInt 실행해도 상관은 없다 /두개의 값을 넣기위해서는 set이라고 시작해야한다
			//숫자 그외값 : '' 붙여야한다   
			//여기까지 와야 완성이 SQL 완성이된다.
			
			// 4. SQL 실행하기
			int result = ps.executeUpdate();  //결과가 반환되는것 뭐가? 정수(몇행값) = 해당 테이블 레코드 다 업데이트 되었으니까 그 테이블에 행 갯수가 반환된다 
			// 5. 결과 출력하기
			System.out.println("insert 결과: " +result);
			
			/************************ SELECT ****************************/
			// 3. Statement or PreparedStatment 객체 생성하기
			stmt = conn.createStatement();
			// 4. SQL 실행하기
			String select = "SELECT student_id, student_name, student_dept FROM students"; //완성된것을 줘야한다
			rs =stmt.executeQuery(select);
			// 5. 결과 출력하기 //무조건 while문 
			while(rs.next()) { //rs.next() 값이 있을때만 움직인다 몇번쨰만 가져오고싶다 이건 힘들다 일단 다들고와서 필요한거 골라서 해야한다.
				int id = rs.getInt("student_id");
				String name = rs.getString("student_name");
				String dept = rs.getString("student_dept");
				
				System.out.printf("학번 : %d, 이름: %s, 학년: %s \n", id,name,dept);
			}
			

			/************************ UPDATE ****************************/
			
			// 3. Statement or PreparedStatment 객체 생성하기
			String update ="UPDATE students SET student_dept = ? WHERE student_id = ?";
			ps =conn.prepareStatement(update);
			ps.setInt(2, 110); //몇번째 물음표인지 확인후 그 칸에 넣어준다
			ps.setString(1, "3학년");

			// 4. SQL 실행하기
			result = ps.executeUpdate();
			
			// 5. 결과 출력하기
			System.out.println("update 결과 :" +result);
		
			
		
			/************************ DELETE ****************************/
		
			// 3. Statement or PreparedStatment 객체 생성하기
			stmt = conn.createStatement();
			
			// 4. SQL 실행하기
			String delete = "DELETE FROM students WHERE student_id = " + 110;  //WHERE PK조건으로 된것만 모두삭제이다
			//실행하면(출력하면) :DELETE FROM students WHERE student_id = 110 이거이다.
			result=stmt.executeUpdate(delete); 
			// 5. 결과 출력하기
			System.out.println("delete 결과 : " + result);
		

		} catch (ClassNotFoundException e) { // 예외가발생했습니다
			System.out.println("JDBC Driver Loding Fail");
		} catch (SQLException e) { // 메세지만 간단하게 출력한다 여기서도 예외 발생함
			System.out.println("SQL관련 예외 :" + e.getMessage());
			e.printStackTrace(); //몇번 줄 오류인거 알려주는거 
		} catch (Exception e) {
			// 기타 예외가 등장한다 / 모든 class object(기본값) 될수있다
			e.printStackTrace();
		} finally {
			try {
				// 자원을 해제하는것
				if (rs != null)
					rs.close();
				if (stmt != null)
					stmt.close();
				if (ps != null)
					ps.close();
				if (conn != null)
					conn.close();
			} catch (SQLException e) {
				System.out.println("정상적으로 자원이 해제되지 않았습니다.");
			}
		}
	}

}
