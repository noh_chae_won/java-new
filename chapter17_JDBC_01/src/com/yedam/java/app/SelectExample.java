package com.yedam.java.app;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class SelectExample {

	public static void main(String[] args) {
		try {
		//1. JDBC Driver 로딩하기.
		Class.forName("org.sqlite.JDBC"); //문자열에서 오타나면 못읽고 오류 안잡아준다

		//2. DB에 서버 접속하기(경로정해짐)
		String url = "jdbc:sqlite:/D:/dev/database/testdatabase.db";  //절대경로로 파일 불러오면된다 
		Connection conn = DriverManager.getConnection(url);
		
		//3. Statement or PreparedStatement 객체 생성하기
		//왔다갔다하는 통로 라고 생각하면된다 new연잔사 안쓴다
		Statement stmt = conn.createStatement(); //배달을 준비하고있는거
		
		//4. SQL 실행하기
		ResultSet rs = stmt.executeQuery("SELECT student_id, student_name, student_dept FROM students");
		
		//5. 결과값 출력하기
		//확인하는 방식 조금 다르다 for문 사용 못함 
		//각각에 필드값 가져올떄 
		while(rs.next()) {
			int sId = rs.getInt("student_id");
			String sName =rs.getString("student_name");
			String sDept =rs.getString("student_dept");
			System.out.println("학번 : " +sId);
			System.out.println("이름 : " +sName);
			System.out.println("학번 :" + sDept);
		}
		}catch(ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}finally {
		//6. 자원해제하기 순서 중요☆(반대로 하면된다) ResultSet -> Statement -> Connection
			if(rs != null) rs.close();  //null이 아닐경우 종료시킨다
			if(stmt != null) stmt.close();
			if(conn != null) conn.close();
		}

		

	}

}
