package com.yedam.java.common;

import java.util.Scanner;


import com.yedam.java.members.MemberDAO;
import com.yedam.java.members.Members;

//로그인 정보 싱글톤 사용(필드) 
public class Login {
	private static  Members loginInfo = null;
	private Scanner sc = new Scanner(System.in);
	public static Members getLoginInfo() {
		return loginInfo;
	}
	
//로그인 메뉴 
public Login() {
	while(true) {
		menuPrint();			
		int menuNo = menuSelect();			
		if(menuNo == 1) {
			//로그인
			login();
		}else if(menuNo == 2) {
			//종료
			exit();
			break;
		}else {
			showInputError();
		}
	}
}

//로그인 출력프린트
private void menuPrint() {
	System.out.println("==============");
	System.out.println("1.로그인  2.종료");
	System.out.println("==============");
}

//메뉴 번호선택 
private int menuSelect() {
	int menuNo = 0;
	try {
		menuNo = Integer.parseInt(sc.nextLine());
	}catch(NumberFormatException e) {
		System.out.println("숫자 형식으로 입력해주세요.");
	}
	return menuNo;
}

private void exit() {
	System.out.println("프로그램을 종료합니다.");
}

private void showInputError() {
	System.out.println("메뉴를 확인해주시기 바랍니다.");
}

private void login() {
	//아이디와 비밀번호 입력
	Members inputInfo = inputMember();
	//로그인 시도
	loginInfo = MemberDAO.getInstance().selectOne(inputInfo);
	//실패할 경우 그대로 메소드 종료
	if(loginInfo == null) return;
	
	//성공할 경우 프로그램을 실행 -> 실핼프로그램 만들기 
	//new Management().run();
	System.out.println("로그인 성공");
	new WhoLogin().run(); //메뉴연동
}

private Members inputMember() {
	Members info = new Members();
	System.out.print("아이디 > ");
	info.setMemberId(sc.nextLine());
	System.out.print("비밀번호 > ");
	info.setMemberPassword(sc.nextLine());
	
	return info;
}
}
