package com.yedam.java.common;

import java.util.ArrayList;
import java.util.List;

//인터페이스 상세기능: singletone, trylcatchfinally
public class UsersDAOmpl extends DAO implements ListDAO{
										
	//싱글톤                  instance :클래스로부터 만들어진 객체
	private static ListDAO instance = null;
	public static ListDAO getinstance() {
		if (instance == null)
			instance = new UsersDAOmpl();
		return instance;

	}
	//회원전체조회    /conn = > JDB연결 
	@Override 
	public List<UsersVO> selectAll() {
		List<UsersVO> list = new ArrayList<>();
		try {
			connect();
			stmt = conn.createStatement();
			String sql = "SELECT * FROM members";
			rs = stmt.executeQuery(sql);

			int count = 0;
			while (rs.next()) {
				UsersVO usersVO = new UsersVO();
				usersVO.setMerId(rs.getString("mer_id"));
				usersVO.setMerName(rs.getString("mer_name"));
				usersVO.setMerTel(rs.getString("mer_tel"));
				usersVO.setMerBir(rs.getString("mer_bir"));
				usersVO.setMerSex(rs.getString("mer_sex"));
				usersVO.setMerAddr(rs.getString("mer_addr"));
				list.add(usersVO);
			}	
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			disconnect();
		}
		return list;
	}

	//회원단건조회  -> 이름으로 검색하여 조회한다 
	@Override                       //usersVO 변수사용
	public List<UsersVO> selectOne(UsersVO usersVO) { 
		UsersVO oneVo1 = null;
		List<UsersVO> listA = new ArrayList<>();
		try {
			connect();
			stmt = conn.createStatement();    							
			String sql = "SELECT * FROM members WHERE  mer_name ='" +  usersVO.getMerName()+"'";
								//FROM -데이터베이스 테이블명 / WHERE- 무엇으러 불러올것이냐!
			rs = stmt.executeQuery(sql); // 에러 : nosultch 라고 뜨면 sql 못불러온다

			while(rs.next()) {
				oneVo1 = new UsersVO();
				oneVo1.setMerId(rs.getString("mer_id"));
				oneVo1.setMerName(rs.getString("mer_name"));
				oneVo1.setMerTel(rs.getString("mer_tel"));
				oneVo1.setMerBir(rs.getString("mer_bir"));
				oneVo1.setMerAddr(rs.getString("mer_addr"));
				oneVo1.setMerAddr(rs.getString("mer_sex"));	
				listA.add(oneVo1);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			disconnect();
		}
		return listA;
	}

	//회원등록 
	@Override
	public void insert(UsersVO usersVO) { 
		try {
			connect();
			String sql = " INSERT    INTO    members   VALUES   ( ? , ? , ? , ? , ? , ? , ? , ? ) ";  // ?(물음표)들어가는 수와 밑에 수와 일치해야한다 
			pstmt = conn.prepareStatement(sql);  //prepareStatement 기능 => Statement 클래스의 기능 향상, sql 불러옴
			pstmt.setInt(1, usersVO.getMerCod());
			pstmt.setString(2, usersVO.getMerId());
			pstmt.setString(3, usersVO.getMerPw());
			pstmt.setString(4, usersVO.getMerName());
			pstmt.setString(5, usersVO.getMerSex());
			pstmt.setString(6, usersVO.getMerBir());
			pstmt.setString(7, usersVO.getMerTel());
			pstmt.setString(8, usersVO.getMerAddr());
			int result = pstmt.executeUpdate();    //1) 결과로 Int타입으로 값을 반환한다 2)SELECT 구문을 제외한 다른 구문을 수행할 때 함수로나온다  

			if (result > 0) {
				System.out.println(" 정상적으로 등록되었습니다. ");
			} else {
				System.out.println(" 정상적으로 등록되지 않았습니다.");
			}
		} catch (Exception e) {
			e.printStackTrace();  //e.printStackTrace() : 에러의 발생근원지를 찾아서 단계별로 에러를 출력해주는것 
		} finally {
			disconnect(); //연결끊음 

		}

	}
	//회원수정 => 이름으로 물어서 수정할수 있는것은 핸드폰과 주소이다 
	@Override
	public void update(UsersVO usersVO) {
		try {
			connect();  
			String sql = "UPDATE members SET mer_addr = ? , mer_tel = ? WHERE mer_name = ? ";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(3, usersVO.getMerName());
			pstmt.setString(1, usersVO.getMerAddr());
			pstmt.setString(2, usersVO.getMerTel());

			int result = pstmt.executeUpdate();

			if (result > 0) {
				System.out.println("정상적으로 수정되었습니다.");
			} else {
				System.out.println("정상적으로 수정되지 않았습니다.");
			}
		} catch (Exception e) {
			e.printStackTrace();  
		} finally {
			disconnect();
		}

	}

	//회원 삭제  -> 회원 아이디로 삭제한다 
	public void delete(String merId) {
		try {
			connect();
			stmt = conn.createStatement();
			String sql = "DELETE FROM members WHERE mer_id = '" + merId+"'";
			int result = stmt.executeUpdate(sql);
			if (result > 0) {
				System.out.println("정상적으로 삭제되었습니다.");
			} else {
				System.out.println("정상적으로 삭제되지 않았습니다.");
			}
		} catch (Exception e) {
			e.printStackTrace(); 
		} finally {
			disconnect();
		}

	}

}

