package com.yedam.java.common;

import java.util.Scanner;

import com.yedam.java.members.MemberManagement;
import com.yedam.java.student.StudentManagement;

//관리자 일반인 로그인 
public class WhoLogin{
	protected Scanner sc = new Scanner(System.in);
	public void run() {
		boolean role = selectRoleA();  //권한주기
		while (true) {
			// 메뉴출력
			menuPrint(role); //로그인으로 권한으로 출력값 다름 
			int menuNO = menuSelect();
			//메뉴선택
			//각 메뉴의 기능을 실행
			if (menuNO == 2 && role ) {
				//관리자(role)제한  
				new MemberManagement();
				} else if(menuNO == 1  ){
				//일반인이 보이는 창이며 수강청메뉴로 간다 
					new StudentManagement();
				}else if(menuNO == 3) {
				// 종료
				end();
				break;
			} else {
				//오류메시지
				showInputErrorMessage();
			}		
	
				}
	}

	//권한 주기: 0 -> 관리자 
	private boolean selectRoleA() {
		
		int memberRole = Login.getLoginInfo().getMemberRole();
							//로그인한 사용자를 반환한다 
		if(memberRole == 0) {
			return true;  
		}else {
			return false;
		}
	}

		
	private void menuPrint(boolean role) {
		//권한에 따른 메뉴
		String menu ="";
		if(role) {
			menu += "2.관리자";  
			
		}
		menu += "1.수강신청 3.종료";  //일반인 출력메뉴  
		
		System.out.println("===========================");
		System.out.println(menu);
		System.out.println("===========================");
		
	}

	//메뉴 번호로 선택 
	private int menuSelect() {
		int menuNo = 0;
		try {
			menuNo = Integer.parseInt(sc.nextLine());
		}catch(NumberFormatException e) {
			System.out.println("숫자 형식으로 입력해주세요.");
		}
		return menuNo;
	}
			
	//오류메시지
	private void showInputErrorMessage() {
		System.out.println("======================");
		System.out.println("메뉴를 다시 한번 확인해주세요.");
		System.out.println("======================");		
	}
	//종료 
	private void end() {
		System.out.println("======================");
		System.out.println("프로그램 종료합니다.");
		System.out.println("======================");	
	}				
		
	}
	
	
	
	
	
	

	
