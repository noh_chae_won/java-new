package com.yedam.java.common;

import java.util.List;

import com.yedam.java.members.MemberDAO;



public interface ListDAO {

		//1)전체 조회 :모든정보   
		List <UsersVO> selectAll();
		
		//2)단건조회 
		List<UsersVO> selectOne(UsersVO users);
		
		//등록 
		void insert(UsersVO users);
		
		//수정
		void update(UsersVO users);
		
		//삭제
		
		void delete(String  merId);

		
		
		
	
}