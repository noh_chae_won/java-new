package com.yedam.java.common;

public class UsersVO {
	//필드
	private int merCod;
	private  String merId;
	private String merPw;
	private String merName;
	private String merSex;
	private String merBir;
	private String merTel;
	private String merAddr;
	
	
	public int getMerCod() {
		return merCod;
	}

	public void setMerCod(int merCod) {
		this.merCod = merCod;
	}

	public  String getMerId() {
		return merId;
	}

	public void setMerId(String merId) {
		this.merId = merId;
	}

	public String getMerPw() {
		return merPw;
	}

	public void setMerPw(String merPw) {
		this.merPw = merPw;
	}

	public String getMerName() {
		return merName;
	}

	public void setMerName(String merName) {
		this.merName = merName;
	}

	public String getMerSex() {
		return merSex;
	}

	public void setMerSex(String merSex) {
		this.merSex = merSex;
	}

	public String getMerBir() {
		return merBir;
	}

	public void setMerBir(String merBir) {
		this.merBir = merBir;
	}

	public String getMerTel() {
		return merTel;
	}

	public void setMerTel(String merTel) {
		this.merTel = merTel;
	}

	public String getMerAddr() {
		return merAddr;
	}

	public void setMerAddr(String merAddr) {
		this.merAddr = merAddr;
	}

	@Override
	public String toString() {
		return "users [merCod=" + merCod + ", merId=" + merId + ", merPw=" + merPw + ", merName=" + merName
				+ ", merSex=" + merSex + ", merBir=" + merBir + ", merTel=" + merTel + ", merAddr=" + merAddr + "]";
	}

	
	
		
	}
	
	
	
	
	


