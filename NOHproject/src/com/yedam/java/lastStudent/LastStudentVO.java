package com.yedam.java.lastStudent;

public class LastStudentVO {
	
	public int lastCod;
	public String lastName;
	public  String lastData;
	
	
	
	
	public int getLastCod() {
		return lastCod;
	}
	public void setLastCod(int lastCod) {
		this.lastCod = lastCod;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getLastData() {
		return lastData;
	}
	public void setLastData(String lastData) {
		this.lastData = lastData;
	}
	
	
	@Override
	public String toString() {
		return "LastStudentVO [lastCod=" + lastCod + ", lastName=" + lastName + ", lastData=" + lastData + "]";
	}
	
	
	
	
	
	
	
}
