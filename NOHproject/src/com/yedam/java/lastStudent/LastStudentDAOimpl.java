package com.yedam.java.lastStudent;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.yedam.java.common.DAO;

public class LastStudentDAOimpl extends DAO implements LastStudentDAO {

	//싱글톤         
			private static LastStudentDAO instance = null;
			
			public static LastStudentDAO getinstance() {
				if (instance == null)
					instance = new LastStudentDAOimpl();
				return instance;
			}

			@Override
			public List<LastStudentVO> selectAllA() {
				List<LastStudentVO>list = new ArrayList<>();
				try {
					connect();// DB와의 연결
					stmt = conn.createStatement();
					String sql = "SELECT * FROM final ";
					rs = stmt.executeQuery(sql); 
					int count = 0;
					while (rs.next()) {
						LastStudentVO lsVO = new LastStudentVO();
						lsVO.setLastCod(rs.getInt("f_code"));
						lsVO.setLastName(rs.getString("f_name"));
						lsVO.setLastData(rs.getString("f_data"));
						list.add(lsVO);

						if (++count >= 20)
							break;
					}
				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					disconnect();
				}
				return list;
			}

			@Override
			public void insert(LastStudentVO lsVO) {
				try {
					String pattern = "yyyy-MM-dd";  //날짜 불러오는 방식 
					SimpleDateFormat simpledateformat =new SimpleDateFormat(pattern);
					String data = simpledateformat.format(new Date());
					connect();
					String sql = " INSERT    INTO    final   VALUES   ( ? , ? , ? ) ";
					pstmt = conn.prepareStatement(sql);
					pstmt.setInt(1, lsVO.getLastCod());
					pstmt.setString(2, lsVO.getLastName());
					pstmt.setString(3, (data));

					int result = pstmt.executeUpdate();
					if (result > 0) {
						System.out.println(" 정상적으로 등록되었습니다. ");
					} else {
						System.out.println(" 정상적으로 등록되지 않았습니다.");
					}
				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					disconnect();

				}
				
			}

			

			@Override
			public void delete(int LastStudentVO) {
				try {
					connect();
					stmt = conn.createStatement();   
					String sql = "DELETE FROM final  WHERE f_code = " + LastStudentVO;
					int result = stmt.executeUpdate(sql);
					if (result > 0) {
						System.out.println("정상적으로 삭제되었습니다.");
					} else {
						System.out.println("정상적으로 삭제되지 않았습니다.");
					}
				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					disconnect();
				}

			}
				
			}
	
			
	
	