package com.yedam.java.lastStudent;

import java.util.List;

public interface LastStudentDAO {
	//조회
	List<LastStudentVO> selectAllA();

	//등록
	void insert(LastStudentVO laVO);
	
	//삭제
	void delete(int lastCod);
	
}
