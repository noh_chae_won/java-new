package com.yedam.java.student;

public class StudentVO{
	private int sCod;
	private String sName;
	private String sTname;
	private String sData;
	private String sTime;
	private String sMony;
	
	
	public int getsCod() {
		return sCod;
	}
	public void setsCod(int sCod) {
		this.sCod = sCod;
	}
	public String getsName() {
		return sName;
	}
	public void setsName(String sName) {
		this.sName = sName;
	}
	public String getsTname() {
		return sTname;
	}
	public void setsTname(String sTname) {
		this.sTname = sTname;
	}
	public String getsData() {
		return sData;
	}
	public void setsData(String sData) {
		this.sData = sData;
	}
	public String getsTime() {
		return sTime;
	}
	public void setsTime(String sTime) {
		this.sTime = sTime;
	}
	public String getsMony() {
		return sMony;
	}
	public void setsMony(String sMony) {
		this.sMony = sMony;
	}
	
	@Override
	public String toString() {
		return "StudentVO [sCod=" + sCod + ", sName=" + sName + ", sTname=" + sTname + ", sData=" + sData + ", sTime="
				+ sTime + ", sMony=" + sMony + "]";
	}
	
	
	
	
}

