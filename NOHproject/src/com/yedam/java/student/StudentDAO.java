package com.yedam.java.student;

import java.util.List;

public interface StudentDAO {

	//전체 조회 :모든정보   
	List<StudentVO> selectAll();
	
	
	//등록 
	void insert(StudentVO studentVO);
	
	//삭제
	
	void delete(String sName); 
}
