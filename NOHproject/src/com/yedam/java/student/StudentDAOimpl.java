package com.yedam.java.student;

import java.util.ArrayList;
import java.util.List;
import com.yedam.java.common.DAO;

public class StudentDAOimpl extends DAO implements StudentDAO {

	//싱글톤         
		private static StudentDAO instance = null;
		
		public static StudentDAO getinstance() {
			if (instance == null)
				instance = new StudentDAOimpl();
			return instance;

		}
	
	@Override
	public List<StudentVO> selectAll() {
		List<StudentVO> list = new ArrayList<>();
		try {
			connect();// DB와의 연결
			stmt = conn.createStatement();
			String sql = "SELECT * FROM sportlist ";
			rs = stmt.executeQuery(sql); 
			int count = 0;
			while (rs.next()) {
				StudentVO studentVO = new StudentVO();
				studentVO.setsCod(rs.getInt("sp_code"));
				studentVO.setsName(rs.getString("sp_sname"));
				studentVO.setsTname(rs.getString("sp_tname"));
				studentVO.setsData(rs.getString("sp_data"));
				studentVO.setsTime(rs.getString("sp_time"));
				studentVO.setsMony(rs.getString("sp_mony"));
				list.add(studentVO);

				if (++count >= 20)
					break;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			disconnect();
		}
		return list;
	}
	

	
	@Override
	public void insert(StudentVO studentVO) {
		try {
			connect();
			String sql = " INSERT    INTO    final   VALUES   ( ? , ? , ?) ";
			pstmt = conn.prepareStatement(sql);
			
			pstmt.setInt(1, studentVO.getsCod());
			pstmt.setString(2, studentVO.getsName());
			pstmt.setString(3, studentVO.getsTname());
			pstmt.setString(4, studentVO.getsData());
			pstmt.setString(5, studentVO.getsTime());
			pstmt.setString(6, studentVO.getsMony());

			int result = pstmt.executeUpdate();
			if (result > 0) {
				System.out.println(" 정상적으로 등록되었습니다. ");
			} else {
				System.out.println(" 정상적으로 등록되지 않았습니다.");
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			disconnect();

		}
		
	}

	@Override
	public void delete(String sName) {
		try {
			connect();
			stmt = conn.createStatement();   
			String sql = "DELETE FROM sportlist  WHERE sp_sname = '" + sName+"'";
			int result = stmt.executeUpdate(sql);
			if (result > 0) {
				System.out.println("정상적으로 삭제되었습니다.");
			} else {
				System.out.println("정상적으로 삭제되지 않았습니다.");
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			disconnect();
		}

	}


		
	}
	
	    
	
		
	
			

	
			

