package com.yedam.java.student;

import java.util.List;
import java.util.Scanner;

import com.yedam.java.common.Login;
import com.yedam.java.common.WhoLogin;
import com.yedam.java.lastStudent.LastStudentDAO;
import com.yedam.java.lastStudent.LastStudentDAOimpl;
import com.yedam.java.lastStudent.LastStudentVO;
import com.yedam.java.members.MemberDAO;
import com.yedam.java.members.Members;


public class StudentManagement extends WhoLogin{
	Scanner sc = new Scanner(System.in);
	protected StudentDAO studentDAO = StudentDAOimpl.getinstance();
	//protected MemberDAO memberDAO = MemberDAO.getInstance();
	LastStudentDAO lsDAO = LastStudentDAOimpl.getinstance();
	
	public  StudentManagement() {
		while (true) {
			// 메뉴출력
			menuPrint();
			//메뉴선택
			int menuNo = menuSelect();
			//각 메뉴의 기능을 실행 
			if(menuNo == 1) {
				//수강조회
				selectAll();
			}else if(menuNo == 2) {
				//나의수강조회
				selectAllA();
			}else if(menuNo == 3  ) {
				//수강신청
				insertEmp();
			}else if(menuNo == 4 )  {
				//수강삭제
				deleteEmp();
			}else if(menuNo == 9 ) {
				end();
				break;
			}else {
				//기타사항 
			printErrorMessage();
		}
	}
}
			
			
	private void menuPrint() {
		System.out.println("=================================================================");
		System.out.println(" 1. 강습전체조회 2. 나의강습조회 3. 강습신청 4. 강습취소 | 9.종료");
		System.out.println("=================================================================");
		
	}



	private int menuSelect() {
		int menuNo = 0;
		try {
			menuNo = Integer.parseInt(sc.nextLine());
		} catch (NumberFormatException e) {
			System.out.println("메뉴는 숫자로 구성되어있습니다.");
		}
		return menuNo;
	}


	private void printErrorMessage() {
		System.out.println("======================");
		System.out.println("메뉴를 잘못 입력하였습니다.");
		System.out.println("메뉴를 다시 한번 확인해주세요.");
		System.out.println("======================");
		
	}

	private void end() {
		System.out.println("======================");
		System.out.println("메뉴로 돌아갑니다.");
		System.out.println("======================");
	}

	private void deleteEmp() {
		int lsVO =  inputcourseDel();
		lsDAO.delete(lsVO);
	}
	
	private int inputcourseDel() {
		int lastCod = 0;
		try {
			System.out.println("강좌번호 > ");
			lastCod = Integer.parseInt(sc.nextLine());
		}catch(NumberFormatException e) {
			System.out.println("강좌번호는 숫자로 구성되어 있습니다.");
		}
		return lastCod;
	}


	private String inputEmpInfoId() {
		System.out.println("수강명 > ");
		String sName = (sc.nextLine());
		return sName;
	}



	private void insertEmp() {
//		StudentVO studentVO = inputEmpAll();
//		studentDAO.insert(studentVO);;
		LastStudentVO laVO = inputEmpAll();
		Members loginInfo = Login.getLoginInfo();
		laVO.setLastName(loginInfo.getMemberName());
		laVO.setLastData(laVO.getLastData());
		//DB에 저장
		lsDAO.insert(laVO);
	}


	private LastStudentVO inputEmpAll() {
		LastStudentVO lsVO= new LastStudentVO();
		System.out.print("수강번호 >  ");
		lsVO.setLastCod(Integer.parseInt(sc.nextLine()));
		return lsVO;
	}

	private void selectAll() { // 단순히 전체값을 가져오기
		List<StudentVO> list = studentDAO.selectAll();
		if(list.isEmpty()) {
			System.out.println("정보가 존재하지 않습니다.");
			return;
		}
			for(StudentVO studentVO : list) {
				
				System.out.printf("%d: %s, %s, %s, %s \n", studentVO.getsCod(),studentVO.getsName(),studentVO.getsTname(),studentVO.getsTime(),studentVO.getsMony());
	}
	}
	
	private void selectAllA(){
		List<LastStudentVO> list = lsDAO.selectAllA();
		if(list.isEmpty()) {
			System.out.printf("%d 수강강습이 존재하지 않습니다. \n");
			return;
		}
		for(LastStudentVO lsVO : list) {
			System.out.printf("수강코드 %s, 수강이름 %s, 수강날짜 %s\n", lsVO.getLastCod(), lsVO.getLastName(), lsVO.getLastData());
		}
		
	}
	
	
	
}