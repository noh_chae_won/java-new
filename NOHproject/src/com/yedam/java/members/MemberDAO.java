package com.yedam.java.members;

import java.sql.SQLException;

import com.yedam.java.common.DAO;
                           
public class MemberDAO extends DAO {
     					//상속
	//싱글톤
		private static MemberDAO dao = null;
		public static MemberDAO getInstance() {
			if(dao == null) {
				dao = new MemberDAO();
			}
			return dao;
		}
		
		//아이디로 인해 회원단건조회 
		public Members selectOne(Members members) {
			Members loginInfo = null;
			try {
				connect();
				String sql = "SELECT * FROM members WHERE mer_id = '" + members.getMemberId() +"'"; 
				stmt = conn.createStatement();  //데이터베이스로 SQL 문을 보내기 위한 SQLServerStatement 개체
				rs = stmt.executeQuery(sql);  //메서드는 데이터베이스에서 데이터를 가져와서 결과 반환합니다. 이 메서드는 Select 문에서만 실행된다.
				
				if(rs.next()) {
					//아이디 존재                         스트링값이라서 
					if(rs.getString("mer_pw").equals(members.getMemberPassword())){
						 //equals 는 Object에 포함되어 있기 때문에 모든 하위 클래스에서 재정의 해서 사용이 가능
						//비밀번호 일치
						// -> 로그인 성공
						loginInfo = new Members();
						loginInfo.setMemberId(rs.getString("mer_id")); //set: 입력  <id와 pw 입력 후 로그인> 
						loginInfo.setMemberPassword(rs.getString("mer_pw"));
						loginInfo.setMemberRole(rs.getInt("mer_code"));
						loginInfo.setMemberName(rs.getString("mer_name"));
					}else {
						System.out.println("비밀번호가 일치하지 않습니다.");
					}
				}else {
					System.out.println("아이디가 존재하지 않습니다.");
				}
				
			}catch(SQLException e) {
				e.printStackTrace();
			}finally {
				disconnect();
			}	
			return loginInfo;
		}
		
		//try블록에는 예외가 발생할 수 있는 코드가 위치합니다. 
		//try 블록의 코드가 예외없이 정상 실행되면 catch블록의 코드는 실행되지 않고 finally 블록의 코드를 실행합니다. 
		//하지만 try 블록의 코드에서 예외가 발생하면 즉시 실행을 멈추고 catch 블록으로 이동하여 예외처리 코드를 실행합니다. 
		//그리고 finally 블록의 코드를 실행합니다. 
		
}