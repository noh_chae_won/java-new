package com.yedam.java.members;

import java.util.List;
import java.util.Scanner;

import com.yedam.java.common.ListDAO;
import com.yedam.java.common.UsersDAOmpl;
import com.yedam.java.common.UsersVO;
import com.yedam.java.common.WhoLogin;

//중간로그인 상속받음  
public class MemberManagement extends WhoLogin {

	Scanner sc = new Scanner(System.in);
	protected ListDAO listDAO = UsersDAOmpl.getinstance();
	protected MemberDAO memberDAO = MemberDAO.getInstance();

	public MemberManagement() {
		while (true) {
			// 메뉴출력
			menuPrint();
			// 메뉴선택
			int menuNo = menuSelect();
			// 각 메뉴의 기능을 실행
			if (menuNo == 1) {
				// 조회
				selectAll();
			} else if (menuNo == 2) {
				// 조회
				selectOne();
			} else if (menuNo == 3) {
				// 등록
				insertEmp();
			} else if (menuNo == 4) {
				// 수정
				updateEmp();
			} else if (menuNo == 5) {
				// 삭제
				deleteEmp();
			} else if (menuNo == 9) {
				end();
				break;
			} else {
				// 기타사항
				printErrorMessage();
			}

		}
	}

	public void menuPrint() {
		System.out.println("=========================================================");
		System.out.println("1.회원전체조회 2.회원확인조회 3.회원등록 4. 회원수정 5.회원삭제 9.종료");
		System.out.println("=========================================================");
	}

	private int menuSelect() {
		int menuNo = 0;
		try {
			menuNo = Integer.parseInt(sc.nextLine());
		} catch (NumberFormatException e) {
			System.out.println("메뉴는 숫자로 구성되어 있습니다.");
		}
		return menuNo;
	}

	private UsersVO inputEmpAll() {
		UsersVO usersVO = new UsersVO();
		System.out.println("회원코드 > ");
		usersVO.setMerCod(Integer.parseInt(sc.nextLine()));
		System.out.println("회원아이디 > ");
		usersVO.setMerId(sc.nextLine());
		System.out.println("회원비밀번호 > ");
		usersVO.setMerPw(sc.nextLine());
		System.out.println("회원이름 >");
		usersVO.setMerName(sc.nextLine());
		System.out.println("성별(여,남)");
		usersVO.setMerSex(sc.nextLine());
		System.out.println("생년월일(yyyy-MM-dd) > ");
		usersVO.setMerBir(sc.nextLine());
		System.out.println("회원 핸드폰번호(yyy-MMMM-dddd) >");
		usersVO.setMerTel(sc.nextLine());
		System.out.println("회원 주소 >");
		usersVO.setMerAddr(sc.nextLine());
		return usersVO;
	}

	private void selectAll() {
		List<UsersVO> list = listDAO.selectAll();
		if (list.isEmpty()) {
			System.out.println("회원정보가 존재하지 않습니다.");
			return;
		}
		for (UsersVO usersVO : list) {
			System.out.printf("%s: %s, %s, %s, %s, %s \n", usersVO.getMerId(), usersVO.getMerName(),
					usersVO.getMerTel(), usersVO.getMerBir(), usersVO.getMerSex(), usersVO.getMerAddr());
		}

	}

	private void selectOne() {
		UsersVO finOne = inputEmpInfo1();
		List<UsersVO> usersVO = listDAO.selectOne(finOne);
		if (usersVO.isEmpty()) {
			System.out.printf("%s 회원은 존재하지 않습니다. \n", finOne.getMerName());
		} else {
			System.out.printf("검색결과 > ");
			System.out.println(usersVO);
		}

	}

	private void insertEmp() {
		UsersVO usersVO = inputEmpAll();
		listDAO.insert(usersVO);

	}

	private void updateEmp() {
		UsersVO usersVO = inputEmpInfo2();
		listDAO.update(usersVO);

	}

	private UsersVO inputEmpInfo2() {   
		UsersVO usersVO = new UsersVO();
		System.out.println("회원이름 > ");
		usersVO.setMerName(sc.nextLine());
		System.out.println("회원전화번호 > ");
		usersVO.setMerTel(sc.nextLine());
		System.out.println("회원주소 > ");
		usersVO.setMerAddr(sc.nextLine());
		return usersVO;
	}

	private UsersVO inputEmpInfo1() {
		UsersVO usersVO = new UsersVO();
		System.out.println("회원이름 > ");
		usersVO.setMerName(sc.nextLine());
		return usersVO;
	}

	private void deleteEmp() {
		String merId = inputEmpInfoId();
		listDAO.delete(merId);
	}

	private String inputEmpInfoId() {
		System.out.println("회원아이디 > ");
		String merid = (sc.nextLine());
		return merid;

	}

	private void end() {
		System.out.println("======================");
		System.out.println("프로그램 종료합니다.");
		System.out.println("======================");

	}

	private void printErrorMessage() {
		System.out.println("======================");
		System.out.println("메뉴를 잘못 입력하였습니다.");
		System.out.println("메뉴를 다시 한번 확인해주세요.");
		System.out.println("======================");
	}

}



