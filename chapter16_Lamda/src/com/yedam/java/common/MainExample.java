package com.yedam.java.common;

public class MainExample {

	public static void main(String[] args) {
		Functionalnterface mfi1 =new MyfunctionClass();
		mfi1.method();
		
		MyfunctionClass mic1 =new MyfunctionClass();
		mic1.func();
		
		//익명 구현 객체 
		//1) 상속 혹은 구현의 관계에서 가능
		//2) 재사용 불가
		//3) 고유의 메소드를 
		Functionalnterface mfi2 = new Functionalnterface() {

			@Override
			public void method() {
				System.out.println("익명 구현 객체를 통해 실현");
				
			}
			public void func() {
				System.out.println("익명 구현 객체 소유");
			}
			
		};
		
		mfi2.method();
		
		MyfunctionClass mfc2 = new MyfunctionClass() {
			@Override
			public void method() {
				System.out.println("부모 클래스 상속 후 오버라이딩 ");
			};
		
			@Override
			public void func() {
				System.out.println("부모 클래스 상속 후 오버라이딩222");
			}
			
		};
		mfc2.method();
		mfc2.func();
		
	}

}
