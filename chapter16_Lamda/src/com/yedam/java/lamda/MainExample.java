package com.yedam.java.lamda;

public class MainExample { //람다식 : 함수를 정의한다 

	public static void main(String[] args) {
		MyFuncInterface fi1 = () -> {System.out.println("람다식 메소드 구현");};
		fi1.method();								//하나의 추상메소드
		
		MyFuncInterface fi2 = new MyFuncInterface() {

			@Override
			public void method() {
				System.out.println("익명 구현 객체 메소드 구현");
				
			}
			
		};
		fi2.method();
		
		fi1 = () -> System.out.println("람다식 메소드 구현 시 실행구분 생략");
		fi1.method();
	}

}
