package com.yedam.java.lamda;

@FunctionalInterface  //실제적으로 기능은 없는데 얘가 없으면 추상메소드에 하나만 있습니다 보장못한다 
public interface MyFuncInterface {
	public void method();
	//public void method2();
}
