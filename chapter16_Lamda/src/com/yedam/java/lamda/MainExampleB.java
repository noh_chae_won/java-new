package com.yedam.java.lamda;

public class MainExampleB {
	public static void main(String[] args) {
		MyInterfaceB fib = (x, y)-> {int result = x + y;
										return result;};
										
		System.out.println(fib.method(10, 25));
		
		fib=(x,y) -> {return x=y;};
		
		fib = (x,y) -> x + y; //위에랑 같은 의미 
	
		fib =(x, y) -> sum(x,y);
		//fib =(x, y) -> {return sum(x,y);}; 이다 
	}
	
	public static int sum(int x, int y) {
		return x + y;
	}
}
