package com.yedam.app.common;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DAO {
	//DB에서 접속하는것과 DB를 분리 시킬것이다. 
	//DAO : 데이터 접근할때 쓰이는 객체 (database 연결 정보) 무조건★★
	//EmpDAo : employees 로 대체되면서 만들어 놓는것이다
	private String jdbcDriver = "org.sqlite.JDBC";
	private String jdbcUrl = "jdbc:sqlite:/D:/dev/Memberdatabase.db";
	
	// 각 메소드에서 공통적으로 사용하는 변수 -> 필드
	//상속으로 전달할것이다
	protected Connection conn;
	protected Statement stmt;
	protected PreparedStatement pstmt;
	protected ResultSet rs;
	
	//연결 
	//1. JDBC DRIVER LOADING 
	//2. CONNECTION
	public void connect() {
		try {
			Class.forName(jdbcDriver);
			
			conn = DriverManager.getConnection(jdbcUrl);
		}catch(ClassNotFoundException e) {
			System.out.println("JDBC DRIVAER LOADING FAIL");
		}catch(SQLException e) {
			System.out.println("DATABASE CONNECTION FAIL");
		}
	}
	//연결해제
	//6.자원해제
	public void disconnect() {
		try {
			if(rs != null) rs.close();
			if(stmt != null)stmt.close();
			if(pstmt != null)pstmt.close();
			if(conn !=null)conn.close();
		}catch(SQLException e){
			System.out.println();
		}
	}
	
}
