package com.yedam.app.emp;

import java.util.List;

public interface EmpDAO {

	//조회 
	//1)전체 조회 :모든정보   
	List <EmpVO> selectAll();
	
	//2) 단건조회  
	EmpVO selectOne(EmpVO empVO);
	
	//등록
	void insert(EmpVO empVO);
	
	//수정
	void update(EmpVO empVO);
	
	//삭제
	void delete(int empNO);
}
