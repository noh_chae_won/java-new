//package com.yedam.app.emp;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import com.yedam.app.common.DAO;
//
//public class EmpDAOImpl extends DAO implements EmpDAO {
//	//싱글톤 
//	private static EmpDAO instance = null;
//	
//	//인터페이스를 이용해서 구현클래스 쓴다
//	public static EmpDAO getInstance() {
//		if(instance == null)
//			instance =new EmpDAOImpl();
//		return instance;
//	}
//	
//	@Override
//	public List<EmpVO> selectAll() {
//		List<EmpVO> list = new ArrayList<>();
//		try {
//			connect(); //DB랑 연결 메뉴하나 실행하고나서 다음 메뉴 실행할때 그 시간이 얼마나 걸릴지 모르니 그 시간동안 허비하지말고 연결해주며 다하면 끊어준다 
//			String sql = "SELECT *FROM emplotees";  
//			rs = stmt.executeQuery(sql);
//			int count = 0;
//			while(rs.next()) {
//				EmpVO empVO = new EmpVO();
//				empVO.setEmpNo(rs.getInt("emp_no"));
//				empVO.setBirthDate(rs.getNString("birth_date"));
//				empVO.setFirstName(rs.getNString("first_name"));
//				empVO.setLastName(rs.getString("last_name"));
//				empVO.setGender(rs.getString("gender"));
//				empVO.setHireDate(rs.getNString("hire_date"));
//				
//				list.add(empVO);
//				
//				if(++count >= 20) break;
//			}
//		} catch (Exception e) {
//			e.printStackTrace();
//		} finally {
//			disconnect(); //자원해제
//		}
//		return list;  //이것땜에 오류 많이 난다 잘 적기 
//	}
//
//	@Override
//	public EmpVO selectOne(EmpVO empVO) {
//		EmpVO findVO= null;
//		try {
//			connect();
//			stmt = conn.createStatement();
//			
//			String sql = "SELECT *FROM emplotees WHETE emp_no = " +empVO;
//			rs =stmt.executeQuery(sql);
//			
//			if(rs.next()) {
//				findVO = new EmpVO();
//				findVO.setEmpNo(rs.getInt("emp_no"));
//				findVO.setBirthDate(rs.getString("birth_date"));
//				findVO.setFirstName(rs.getString("first_name"));
//				findVO.setLastName(rs.getString("last_name"));
//				findVO.setGender(rs.getNString("gender"));
//				findVO.setHireDate(rs.getString("hire_date"));
//			}
//		}catch(Exception e){
//			e.printStackTrace();
//		}finally {
//			disconnect();
//		}
//		return null;
//	}
//
//	@Override
//	public void insert(EmpVO empVO) {
//		try {
//			connect();
//			String sql = "INSERT INTO employees VALUES (?,?,?,?,?,?)";
//			pstmt = conn.prepareStatement(sql);
//			pstmt.setInt(1, empVO.getEmpNo());
//			pstmt.setString(2, empVO.getBirthDate());
//			pstmt.setString(3, empVO.getFirstName());
//			pstmt.setString(4, empVO.getLastName());
//			pstmt.setString(5, empVO.getGender());
//			pstmt.setString(6, empVO.getHireDate());
//			
//			int result = pstmt.executeUpdate();
//			
//			if(result > 0 ) {
//				System.out.println("정상적으로 등록되었습니다.");
//			}else{
//				System.out.println("정상적으로 등록되지 않았습니다.");
//			}
//		}catch(Exception e) {
//			e.printStackTrace();
//		}finally {
//			disconnect();
//		}
//
//	}
//
//	@Override
//	public void update(EmpVO empVO) {
//		try {
//			connect();
//			String sql ="UPDATE employees SET first_name = ? WHERE emp_mo = ? ";
//			pstmt = conn.prepareStatement(sql);
//			pstmt.setString(1, empVO.getFirstName());
//			pstmt.setInt(2, empVO.getEmpNo());
//			
//			int result = pstmt.executeUpdate();
//			
//			if(result > 0 ) {
//				System.out.println("정상적으로 수정되었습니다.");
//			}else{
//				System.out.println("정상적으로 수정되지 않았습니다.");
//			}
//		}catch(Exception e){
//			e.printStackTrace();
//		}finally {
//			disconnect();
//		}
//
//	}
//
//	@Override
//	public void delecte(int empNO) {
//		try {
//			connect();
//			stmt = conn.createStatement();
//			String sql = "DELETE FROM employees WHERE emp_name = " + empNO;
//			
//			int result = pstmt.executeUpdate();
//			if(result > 0 ) {
//				System.out.println("정상적으로 삭제되었습니다.");
//			}else{
//				System.out.println("정상적으로 삭제되지 않았습니다.");
//			}
//		}catch(Exception e) {
//			e.printStackTrace();
//		}finally {
//			disconnect();
//		}
//
//	}
//
//	
//
//}
