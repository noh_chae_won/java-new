package com.yedam.java.app;

import java.util.List;
import java.util.Scanner;

import com.yedam.app.emp.EmpDAO;
import com.yedam.app.emp.EmpDAOlmpl_1;
import com.yedam.app.emp.EmpVO;

public class EmpManagement_1 {


		Scanner sc = new Scanner(System.in);
		EmpDAO empDAO = EmpDAOlmpl_1.getinstance();
		
		public void EmpManagement() {
			while (true) {
				//메뉴 출력
				menuPrint();
				
				//메뉴 선택
				int menuNo = menuSelect();
				
				//각 메뉴의 기능을 실행
				if(menuNo == 1) {
					//전체조회
					menuSelect();
				}else if(menuNo == 2) {
					//전체조회
					selectAll();
				}else if(menuNo == 3) {
					//단건조회
					selectOne();
				}else if(menuNo == 4) {
					//사원등록
					updateEmp();
				}else if(menuNo == 5) {
					//사원삭제
					deleteEmp();
				}else if(menuNo == 9) {
					end();
					break;
			}else {
				//기타사항 
				printErrorMessage();
				}
				
			}
		}
	
	
		private void printErrorMessage() {
			System.out.println("+++++++++++++++++++++++++++++++++++++");
			System.out.println("메뉴를 잘못 입력 했습니다.");
			System.out.println("메뉴를 다시 한 번 확인해 주세요");
			System.out.println("+++++++++++++++++++++++++++++++++++++");
		}
		
		private void end() {
			System.out.println("+++++++++++++++++++++++++++++++++++++");
			System.out.println("프로그램을 종료 합니다.");
			System.out.println("+++++++++++++++++++++++++++++++++++++");
		}
		
		private void menuPrint() {
			System.out.println("1.전체사원 2.부서번호 3.사원등록 4.사원수정 5.사원삭제 9.종료|||||||");
		}
		
		private int menuSelect() {
			int menuNo = 0;
			try {
				menuNo = Integer.parseInt(sc.nextLine());
			}catch(NumberFormatException e) {
				System.out.println("메뉴는 숫자로 구성되어 있습니다.");
			}
			return menuNo;
		}
		
		private void selectAll() {
			List<EmpVO> list = empDAO.selectAll();
			if(list.isEmpty()) {
				System.out.println("정보가 존재하지 않습니다");
				return;
			}
			for(EmpVO empVO : list) {
				System.out.printf("%d: %s, %s \n",empVO.getDeptNo(),empVO.getEmpNo());
			}
		}
		
		private void selectOne() {
			EmpVO findEmp = inputEmpInfo();
			EmpVO empVO = empDAO.selectOne(findEmp);
			if(empVO == null) {
				System.out.printf("%d 부서번호 존재하지 않습니다. \n", empVO.getDeptNo());
			}else {
				System.out.println("검색결과 >");
				System.out.println(empVO);
			}
		}
		
		private void insertEmp() {
			EmpVO empVO = inputEmpAll();
			empDAO.insert(empVO);
		}
		
		private void updateEmp() {
			EmpVO empVO = inputEmpInfo();
			empDAO.update(empVO);
		}
		
		private void deleteEmp() {
			int empVO = inputEmpNo();
			empDAO.delete(empVO);//사원삭제
		}
		
		private EmpVO inputEmpAll() {
			EmpVO empVO = new EmpVO();
			System.out.println(" 사원번호 > ");
			empVO.setEmpNo(Integer.parseInt(sc.nextLine()));
			System.out.println(" 부서번호 > ");
			empVO.setDeptNo(sc.nextLine());
			System.out.println(" fromdate > ");
			empVO.setFromDate(sc.nextLine());
			System.out.println(" todate > ");
			empVO.setToDate(sc.nextLine());
			return empVO;
		}
		private EmpVO inputEmpInfo() {
		//	사원번호 이름
			EmpVO empVO = new EmpVO();
			System.out.println("사원번호 >");
			empVO.setEmpNo(Integer.parseInt(sc.nextLine()));
			System.out.println("이름>");
			empVO.setFirstName(sc.nextLine());
			return empVO;
		}
		
		private int inputEmpNo() {
			int empNo = 0;
			try {
				empNo = Integer.parseInt(sc.nextLine());
			}catch(NumberFormatException e) {
				System.out.println("사원번호는 숫자로 구성되어 있습니다.");
			}
			return empNo;
		}

	}

//	private static void printErrorMessage() {
//		// TODO Auto-generated method stub
//		
//	}
//
//	private static void end() {
//		// TODO Auto-generated method stub
//		
//	}
//
//	private static void deleteEmp() {
//		// TODO Auto-generated method stub
//		
//	}
//
//	private static void updateEmp() {
//		// TODO Auto-generated method stub
//		
//	}
//
//	private static void selectOne() {
//		// TODO Auto-generated method stub
//		
//	}
//
//	private static void selectAll() {
//		// TODO Auto-generated method stub
//		
//	}
//
//	private static int menuSelect() {
//		// TODO Auto-generated method stub
//		return 0;
//	}
//
//	private static void menuPrint() {
//		// TODO Auto-generated method stub
//		
//	}


