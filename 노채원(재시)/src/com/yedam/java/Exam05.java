package com.yedam.java;

import java.util.Scanner;

public class Exam05 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		boolean run = true;
		Scanner sc = new Scanner(System.in);
		
		int[] scores =null; //내부에 선언하면 안된다
		int studentNum = 0;
		
		while(run) {
			System.out.println("----------------------------------------");
			System.out.println("1.학생수 |2.점수입력 |3.학생 점수리스트 |4.학생점수 분석 |5.종료");
			System.out.println("----------------------------------------");
			System.out.println("선택>");
			
			
			int selectNo = Integer.parseInt(sc.nextLine());
			
			switch(selectNo) {  
			case 1:
				System.out.println("학생수>");
				studentNum = Integer.parseInt(sc.nextLine());
				scores = new int [studentNum];
				break;
			case 2:
				for(int i=0; i<scores.length; i++) {
					System.out.print("score["+i+"]>");
					int score = Integer.parseInt(sc.nextLine());
					scores[i] =score;
				}
				break;
			case 3:
				for(int i=0; i<scores.length; i++) {
					System.out.printf("점수:%d %d\n", i, scores[i]);
				}
				break;
			case 4: 
				int min = scores[0];
				int max = scores[0];
				for(int i=0; i<scores.length; i++) {
					
					if(max < scores[i]) { 
						max = scores[i];
					}
					if( min > scores[i]) { 
						min = scores[i];
					}
					
				}
				System.out.println("최고 점수:" +max);  
				System.out.println("최저 점수::"+min);
								
	
				break;
			case 5:
				run = false;
				System.out.println("프로그램종료");
				break;
			default:
				System.out.println("정해진 메뉴를 입력하지 않았습니다.");
				System.out.println("다시 입력해주세요.");
			}
			
			
			
		}
	}

}
