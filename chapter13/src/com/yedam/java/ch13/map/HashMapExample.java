package com.yedam.java.ch13.map;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class HashMapExample {

	public static void main(String[] args) {

		Map<String, Integer> map =new HashMap<String, Integer>();
		
		//객체 저장 
		map.put("신용권", 85);
		map.put("홍길동", 90); //밑에랑 동일하여 없어진다 
		map.put("동장군", 80);
		map.put("홍길동", 95);  //나중에 넣은걸로 덮어써서 나온다
		
		System.out.println("총 Entry 수 : " + map.size());
		
		//객체 찾기
		int score = map.get("홍길동");
		System.out.println("\t홍길동 " + score);
		System.out.println();

		// 모든 Key값 가져오기
		Set<String> KeySet = map.keySet();
		Iterator<String> KeyInterator = KeySet.iterator();
		while (KeyInterator.hasNext()) {
			String Key = KeyInterator.next();
			Integer value = map.get(Key);
			System.out.println("\t" + Key + " : " + value);    //넣은것과 다르게 순서를 추출한다 
			
		}

		//객체 삭제
		map.remove("홍길동");
		System.out.println(map.size());
		
		//모든 K, V(Map.entry) 가져와서 하나씩 처리
		Set<Map.Entry<String, Integer>> entrySet = map.entrySet();
		Iterator<Map.Entry<String, Integer>> entryIterator = entrySet.iterator();
		
		while(entryIterator.hasNext()) {
			Map.Entry<String, Integer> entry = entryIterator.next();
			String Key = entry.getKey();
			Integer value = entry.getValue();
			System.out.println("\t" + Key + " : " +value);
		
		}
		System.out.println();
		
		//다른방법이 있다
		for(Map.Entry<String, Integer> temp : map.entrySet()) {
			String Key = temp.getKey();
			Integer value = temp.getValue();
			System.out.println("\t" + Key + " : " +value);
			}
		System.out.println();
		
		map.clear();
		System.out.println("총 entry 수:" + map.size());
		
	}
	

}
