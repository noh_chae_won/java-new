package com.yedam.java.ch13.lifofifo;

import java.util.Stack;

public class StackExample {

	public static void main(String[] args) {
		Stack<Coin> coinBox = new Stack<Coin>();
		//Stack : 입구가 하나인 통

		coinBox.push(new Coin(100));
		coinBox.push(new Coin(50));
		coinBox.push(new Coin(500));
		coinBox.push(new Coin(10));
		
		while(!coinBox.isEmpty()) {
			Coin coin =coinBox.pop();  //pop이기에 맨처음 넣은걸 뺴면 무너진다 그래서 맨마지막부터 
			System.out.println("꺼내온 동전 :" + coin.getValue() + "원");
		}
	}

}
