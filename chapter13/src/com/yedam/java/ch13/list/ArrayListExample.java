package com.yedam.java.ch13.list;

import java.util.ArrayList;
import java.util.List;

public class ArrayListExample {

	public static void main(String[] args) {
		
		List<String> list = new ArrayList<String>();
		//List<E> list2 =new ArrayList<>();  //E : 어떤객체를 넣을지 모르니까 Element(요소) 여러가지 넣을거다(뚜렷하지 않을때)할떄 쓰인다 
		
		//객체 저장 
		list.add("Java"); //list[0] = "Java"; 0번째 들어가는것이다 
		list.add("JDBC");
		list.add("Servlet/JSP");
		list.add(2, "Database");
		list.add("iBATIS");
		
		//배열 list.length() 같은개념
		int size = list.size();
		System.out.println("총 객체수 :" + size);
		System.out.println();
		
		
		String skill = list.get(2); //skill = list[2]
		System.out.println("2: " + skill +"\n");
		
		for(int i = 0; i<list.size(); i++) { //저장된 총 객체수 5번
			String str= list.get(i);
			System.out.println(i+ ":" +str);
		}
		System.out.println();
		
		
		System.out.println("===============================");
		list.remove(2);
		
		for(int i = 0; i<list.size(); i++) { //저장된 총 객체수 5번
			String str= list.get(i);
			System.out.println(i+ ":" +str);
		}
		System.out.println();
		
		
		System.out.println("===============================");
		list.remove(2);
		list.remove("iBATIS");
		
		for(int i = 0; i<list.size(); i++) { //저장된 총 객체수 5번
			String str= list.get(i);
			System.out.println(i+ ":" +str);
		}
		System.out.println();
		

	}

}
