package com.yedam.java.ch13.set;

public class Member {

	public String name;
	public int age;
	
	public Member(String name, int age) {
		this.name = name;
		this.age = age;
	}

	@Override
	public int hashCode() {
		//name =String 
		//name + age 더해가지고 똑같은 객체인지 확인한다 
		//두개 객체 비교할때 해쉬코드에서 각 객체마다 다르다고 했지만 다른객체들이 같겠금 해주는게 우리가 해쉬코드에서 재정의 해준다.
		// int 도 같이 포함해서 확인한다
		//Member member = new Member("yedam" , 1)
		//Member member2 = new Member("yedam" , 1)
		//Member member3 = new Member("yedam" , 4) 얘는 다른객체로 인식한다
		
		//"yedam"  ->2
		// age -> 1
		//최종 hashcode() 2 + 1 = 3 으로 나와서 똑같은 객체로 인식한다 
		
		//"yedam" -> 2
		// age - > 4
		//최종 hashcode() = 2+ 4 = 6 동등객체인지 확인한다 
		
		return name.hashCode() + age;
	}

	@Override
	public boolean equals(Object obj) {
		if(obj instanceof Member) {
			Member member = (Member) obj;
			return member.name.equals(name) && (member.age == age);
		}else {
		return false;
	}

}
}