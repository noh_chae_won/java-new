package com.yedam.java.ch13.set;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class HashSetExample {

	public static void main(String[] args) {
	
		Set<String> set = new HashSet<String>();

		set.add("Java");
		set.add("JDBC");
		set.add("Serval/JSD");
		set.add("Java");
		set.add("iBATIS");
		
		int size = set.size();
		System.out.println("총 객체수: " +size);
		System.out.println("##반복자를 활용한 활용##");
		Iterator<String> iterator = set.iterator();
		while(iterator.hasNext()) {
			String element = iterator.next();
			System.out.println("\t" + element);
		}

		
		set.remove("JDBC");
		set.remove("iBATIS");
		System.out.println("##향상된 FOR문 활용##");
		for(String element : set) {
			System.out.println("\t" +element);
		}
		
		//객체를 제거하고 비움 
		set.clear();
		
		if(set.isEmpty()) {
			System.out.println("비어있음");
		}
		
	}

}
